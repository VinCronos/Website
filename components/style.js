var style = {};
style.header = {
    overflow: 'hidden',
    padding: '5px 10px',
    borderBottom: '1px solid #ff0000',
};
style.logo = {
    color: '#ff0000',
    float: 'left',
    fontSize: '1.5em',
    textAlign: 'center',
    textDecoration: 'none',
};
style.links = {
    color: '#ff0000',
    float: 'right',
    textAlign: 'center',
    padding: '0px 10px',
    textDecoration: 'none',
    fontSize: '1.5em',
};

export default style;
