import Header from './header';
import style from './style';

const Main = (properties) => (
    <div>
        <Header />
        <style global jsx>
            {`
            body {
                background: #212121;
            }
            * {
                color: #ff0000;
                text-decoration: none;
            }
            `}
        </style>
        {console.log('%c------VinCronos------', 'font-size: 1.5em; background-color: #000000; color: #ff0000;') /* eslint-disable-line no-console */}
        <br/>
        {properties.children}
    </div>
);

export default Main;
