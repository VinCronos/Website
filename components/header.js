import style from './style';
import Link from 'next/link';

const Header = () => (
    <div style={style.header}>
        <Link href='/'><a style={style.logo}>VinCronos</a></Link>
        <Link href='/videos'><a style={style.links}>Videos</a></Link>
    </div>
);

export default Header;
